set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR ARM)

set(TOOLCHAIN arm-none-eabi)

find_program(CMAKE_C_COMPILER NAMES ${TOOLCHAIN}-gcc HINTS
    "${TC_PATH}" "${TC_PATH}/bin"
    "$ENV{ARM_NONE_EABI_HOME}" "$ENV{ARM_NONE_EABI_HOME}/bin"
    ENV PATH)

if(NOT CMAKE_C_COMPILER)
	message(FATAL_ERROR "\nC-compiler for ${TOOLCHAIN}-gcc not found!\n Please "
    "provide the option -DTC_PATH=<path_to_dir_with_executable> on cmake invocation,\n"
    "create the environment variable ARM_NONE_EABI_HOME or\n add it to your path!\n")
endif()

get_filename_component(CMAKE_FIND_ROOT_PATH ${CMAKE_C_COMPILER} DIRECTORY)
get_filename_component(CMAKE_FIND_ROOT_PATH ${CMAKE_FIND_ROOT_PATH} DIRECTORY)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

find_program(CMAKE_CXX_COMPILER ${TOOLCHAIN}-g++})

if(CMAKE_VERSION VERSION_LESS 3.6.0)
	include(CMakeForceCompiler)
	CMAKE_FORCE_C_COMPILER(${CMAKE_C_COMPILER} GNU)
	CMAKE_FORCE_CXX_COMPILER(${CMAKE_CXX_COMPILER} GNU)
else()
	# Without that flag CMake is not able to pass test compilation check
	set(CMAKE_EXE_LINKER_FLAGS_INIT "--specs=nosys.specs")
endif()
